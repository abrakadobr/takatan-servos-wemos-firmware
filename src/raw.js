
let {el,mount,setAttr,text} = redom
let vr = el('span')
let hd = el('h4#head',['Takatan Servos ver:',vr])
let dn = el('input#dn',{type:'text'})
let we = el('input#we',{type:'text'})
let wp = el('input#wp',{type:'text'})
let wma = el('input',{name:'wm',type:'radio'})
let wml = el('input',{name:'wm',type:'radio'})
let mi  = el('input#mi',{type:'text'})
let mp  = el('input#mp',{type:'text'})
let mmo = el('input#mmo',{type:'checkbox'})
let wb = el('button','Update Main/WiFi/Master settings')
let ft = el('.ft',[
    el('hr'),
    el('a','Takatan Software and Robotics 2020',{href:'https://takatan.tk',target:'_blank'}),
    el('div',[
        el('span','webpanel builded with: '),
        el('a','RE:DOM',{href:'https://redom.js.org',target:'_blank'}),
        el("span",", "),
        el('a','Milligram',{href:'https://milligram.io',target:'_blank'})
    ])
])

let $Q = (qurl, qmethod, qcb, qdata) => {
    let xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200)
            qcb(xhr.responseText)
    }
    xhr.open(qmethod, qurl, true)
    let dt = new FormData()
    if (qdata)
      for(e in qdata)
        dt.append(e,qdata[e])
    xhr.send(dt)
}



let $USI = (si)=>{}

let srvs = {
  onoff: [],
  ang: [],
  tm: [],
  min: [],
  max: [],
  gt: [],

  els: []
}


let $GT=(i)=>{
  let o = {}
  o['a'+i]=srvs.ang[i].value
  o['t'+i]=srvs.tm[i].value
  $Q('/goto','POST',_=>_!='OK'?alert(_):console.log(_),o)
}


for(let i=0;i<8;i++)
{
  srvs.onoff[i] = el('input',{type:'checkbox'})
  srvs.onoff[i].onclick = _=>$USI(i)
  srvs.ang[i] = el('input.i-s.b-m',{type:'text'})
  srvs.tm[i] = el('input.i-s.b-m',{type:'text'})
  srvs.gt[i] = el('button.b-s.b-r.button-outline','Go')
  srvs.gt[i].onclick = _=>$GT(i)
  srvs.min[i] = el('input.i-s.b-m',{type:'text'})
  srvs.max[i] = el('input.i-s.b-r',{type:'text'})

  srvs.els[i] = el('.container.s-c',[
    el('.row',[
      el('.column',[
        srvs.onoff[i],
        el('label.label-inline','Servo '+(i+1),{for:'so'+i}),
      ])
    ]),
    el('.row.mb',[
      el('.column',[
        el('.i-t.b-l','Go to angle'),
        srvs.ang[i],
        el('.i-t.b-m','for'),
        srvs.tm[i],
        el('.i-t.b-m','1/10sec'),
        srvs.gt[i],
      ])
    ]),
    el('.row.mb',[
      el('.column',[
        el('.i-t.b-l','Min angle'),
        srvs.min[i],
        el('.i-t.b-m','max angle'),
        srvs.max[i]
      ])
    ])
  ])
}

$USI = (si) => {
  if (srvs.onoff[si].checked)
  {
    srvs.ang[si].disabled = false
    srvs.tm[si].disabled = false
    srvs.gt[si].disabled = false
    srvs.min[si].disabled = false
    srvs.max[si].disabled = false
  } else {
    srvs.ang[si].disabled = true
    srvs.tm[si].disabled = true
    srvs.gt[si].disabled = true
    srvs.min[si].disabled = true
    srvs.max[si].disabled = true
  }
}


let sb = el('button','Update Servos settings')
let app = el('#app.container',[
  el('.row',
    el('.column.column-100',[
      hd,
      el('#main',[el('h5','Main'),
        el('.fl',[el('label',{for:"dn"},'Device Name'),dn])
      ]),
      el('#wifi',[el('h5','Network'),
        el('.fl',[el('label','WiFi ESSID',{for:"we"}),we]),
        el('.fl',[el('label','WiFi pass'),wp]),
        el('.fl',[el('label','WiFi mode'),[wma,text('Standalone AP'),wml,text('Local Network')]]),
        el('h5','Master'),
        el('.fl',[el('label','IP'),mi]),
        el('.fl',[el('label','Port'),mp]),
        el('.fl.mb',[el('label.label-inline','Lock'),mmo]),
        wb
      ]),
    ])
  ),
  el('#servos',[el('h5','Servos'),...srvs.els,sb]),
  ft
])
mount(document.body,app)

let updateCFG = ()=>{
  $Q('/cfg','GET',cfgs=>{
    let cfg = JSON.parse(cfgs)
    console.log(['cfg',cfg])
    dn.value = cfg.name
    vr.innerHTML = cfg.ver
    we.value = cfg.wifi.essid
    wp.value = cfg.wifi.pass
    if (cfg.wifi.isAP)
      wma.checked = true
    else
      wml.checked = true
    mi.value = cfg.master.ip
    mp.value = cfg.master.port
    if (cfg.master.lock)
      mmo.checked = true
    for(let i=0;i<8;i++)
    {
      srvs.onoff[i].checked = cfg.servos[i].on
      srvs.ang[i].value = cfg.servos[i].ca
      srvs.tm[i].value = cfg.servos[i].ct
      srvs.min[i].value = cfg.servos[i].mi
      srvs.max[i].value = cfg.servos[i].mx
      $USI(i)
    }
  })
}
wb.onclick = ()=>{
  let fd = {
    dn: dn.value,
    we: we.value,
    wp: wp.value,
    wm: wma.checked?'1':'0',
    mi: mi.value,
    mp: mp.value,
    ml: mmo.checked?'1':'0'
  }
  $Q('/cfgmain','POST',res=>{
    if (res=='OK')
      alert('Saved!')
    else
      alert('Reconnect?')
  },fd)
}
sb.onclick = ()=>{
  let fd = {}
  for (let i=0;i<8;i++)
  {
    fd['o'+i] = srvs.onoff[i].checked ? '1':'0'
    fd['a'+i] = srvs.ang[i].value
    fd['t'+i] = srvs.tm[i].value
    fd['mi'+i] = srvs.min[i].value
    fd['mx'+i] = srvs.max[i].value
  }
  $Q('/cfgs','POST',res=>{
    if (res=='OK')
      alert('Saved!')
    else
      alert('Not saved =(')
    location.reload()
  },fd)
}

updateCFG()


