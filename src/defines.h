#ifndef CTR_DEFINES_H
#define CTR_DEFINES_H

#include <Arduino.h>
#include <Ticker.h>
#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <asyncHTTPrequest.h>

/*
WEMOS MAP
0 = D3
1 = X
2 = D4
3 = RX
4 = D2
5 = D1
6 = X
7 = X
8 = X
9 = X
10 = X
11 = X
12 = D6
13 = D7
14 = D5
15 = D8
16 = LED
*/

// do it for serial debugging
//#define DOSERIAL

#ifdef DOSERIAL
  #define TF_SERIAL_SPEED 115200
#endif

#define PIN_S1 5
#define PIN_S2 4
#define PIN_S3 0
#define PIN_S4 2
#define PIN_S5 14
#define PIN_S6 12
#define PIN_S7 13
#define PIN_S8 15

#define PIN_BLUE 16
// RX PIN
// #define PIN_S9 3
// ANALOG 0 FOR RESET

//number of supported servos
#define TF_SERVOS 8
#define TF_SERVO_MIN_ANGLE -115
#define TF_SERVO_MAX_ANGLE 115

#define TF_DEV_PREFIX "TSwf-"
#define TF_CLICK_TIME 5
#define TF_DEF_ANGLE 30

#define TF_MAX_CONNECT_TRY 60

struct Cfg {
  uint8_t ver;
  char name[32];
  char ssid[32];
  char pass[32];
  bool isAP;
  char masterIP[15];
  int masterPort;
  bool talkOnlyMaster;
  bool servoEnabled[TF_SERVOS];
  int servoClickAngle[TF_SERVOS];
  int servoMin[TF_SERVOS];
  int servoMax[TF_SERVOS];
  uint servoClickTime[TF_SERVOS];
};

void readCfg();
void writeCfg();
String defName();
void defCfg();
String cfgJSON();
void debugCfg(const char * msg);
void restartWiFi();

//void clickFinger(uint8_t fid);
//void pressFinger(uint8_t fid);
//void releaseFinger(uint8_t fid);
//void moveTo(uint8_t fid, int angle);
void goTo(int fid, int angle, int time);




#endif
