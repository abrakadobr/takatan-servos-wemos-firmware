#include "defines.h"

Ticker ticker;
AsyncWebServer  webserver(80);
asyncHTTPrequest httpRequest;

String devID = "";
Cfg cfg;
bool connecting2LN = false;
int steps2LN = 0;
unsigned long last2LN = 0;

bool hardReset = false;
int angles[TF_SERVOS];
int clicks[TF_SERVOS];
int times[TF_SERVOS];
const int pins[TF_SERVOS] = {PIN_S1,PIN_S2,PIN_S3,PIN_S4,PIN_S5,PIN_S6,PIN_S7,PIN_S8};

/*
.fl .lbl {width: 150px;display: inline-block;}
.mb { margin-bottom: 10px; }
*/

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>Takatan Servos</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    /*!
     * Milligram v1.3.0
     * https://milligram.github.io
     *
     * Copyright (c) 2017 CJ Patoilo
     * Licensed under the MIT license
     */

    *,*:after,*:before{box-sizing:inherit}html{box-sizing:border-box;font-size:62.5%}body{color:#606c76;font-family:'Helvetica Neue', 'Helvetica', 'Arial', sans-serif;font-size:1.6em;font-weight:300;letter-spacing:.01em;line-height:1.6}blockquote{border-left:0.3rem solid #d1d1d1;margin-left:0;margin-right:0;padding:1rem 1.5rem}blockquote *:last-child{margin-bottom:0}.button,button,input[type='button'],input[type='reset'],input[type='submit']{background-color:#9b4dca;border:0.1rem solid #9b4dca;border-radius:.4rem;color:#fff;cursor:pointer;display:inline-block;font-size:1.1rem;font-weight:700;height:3.8rem;letter-spacing:.1rem;line-height:3.8rem;padding:0 3.0rem;text-align:center;text-decoration:none;text-transform:uppercase;white-space:nowrap}.button:focus,.button:hover,button:focus,button:hover,input[type='button']:focus,input[type='button']:hover,input[type='reset']:focus,input[type='reset']:hover,input[type='submit']:focus,input[type='submit']:hover{background-color:#606c76;border-color:#606c76;color:#fff;outline:0}.button[disabled],button[disabled],input[type='button'][disabled],input[type='reset'][disabled],input[type='submit'][disabled]{cursor:default;opacity:.5}.button[disabled]:focus,.button[disabled]:hover,button[disabled]:focus,button[disabled]:hover,input[type='button'][disabled]:focus,input[type='button'][disabled]:hover,input[type='reset'][disabled]:focus,input[type='reset'][disabled]:hover,input[type='submit'][disabled]:focus,input[type='submit'][disabled]:hover{background-color:#9b4dca;border-color:#9b4dca}.button.button-outline,button.button-outline,input[type='button'].button-outline,input[type='reset'].button-outline,input[type='submit'].button-outline{background-color:transparent;color:#9b4dca}.button.button-outline:focus,.button.button-outline:hover,button.button-outline:focus,button.button-outline:hover,input[type='button'].button-outline:focus,input[type='button'].button-outline:hover,input[type='reset'].button-outline:focus,input[type='reset'].button-outline:hover,input[type='submit'].button-outline:focus,input[type='submit'].button-outline:hover{background-color:transparent;border-color:#606c76;color:#606c76}.button.button-outline[disabled]:focus,.button.button-outline[disabled]:hover,button.button-outline[disabled]:focus,button.button-outline[disabled]:hover,input[type='button'].button-outline[disabled]:focus,input[type='button'].button-outline[disabled]:hover,input[type='reset'].button-outline[disabled]:focus,input[type='reset'].button-outline[disabled]:hover,input[type='submit'].button-outline[disabled]:focus,input[type='submit'].button-outline[disabled]:hover{border-color:inherit;color:#9b4dca}.button.button-clear,button.button-clear,input[type='button'].button-clear,input[type='reset'].button-clear,input[type='submit'].button-clear{background-color:transparent;border-color:transparent;color:#9b4dca}.button.button-clear:focus,.button.button-clear:hover,button.button-clear:focus,button.button-clear:hover,input[type='button'].button-clear:focus,input[type='button'].button-clear:hover,input[type='reset'].button-clear:focus,input[type='reset'].button-clear:hover,input[type='submit'].button-clear:focus,input[type='submit'].button-clear:hover{background-color:transparent;border-color:transparent;color:#606c76}.button.button-clear[disabled]:focus,.button.button-clear[disabled]:hover,button.button-clear[disabled]:focus,button.button-clear[disabled]:hover,input[type='button'].button-clear[disabled]:focus,input[type='button'].button-clear[disabled]:hover,input[type='reset'].button-clear[disabled]:focus,input[type='reset'].button-clear[disabled]:hover,input[type='submit'].button-clear[disabled]:focus,input[type='submit'].button-clear[disabled]:hover{color:#9b4dca}code{background:#f4f5f6;border-radius:.4rem;font-size:86%;margin:0 .2rem;padding:.2rem .5rem;white-space:nowrap}pre{background:#f4f5f6;border-left:0.3rem solid #9b4dca;overflow-y:hidden}pre>code{border-radius:0;display:block;padding:1rem 1.5rem;white-space:pre}hr{border:0;border-top:0.1rem solid #f4f5f6;margin:3.0rem 0}input[type='email'],input[type='number'],input[type='password'],input[type='search'],input[type='tel'],input[type='text'],input[type='url'],textarea,select{-webkit-appearance:none;-moz-appearance:none;appearance:none;background-color:transparent;border:0.1rem solid #d1d1d1;border-radius:.4rem;box-shadow:none;box-sizing:inherit;height:3.8rem;padding:.6rem 1.0rem;width:100%}input[type='email']:focus,input[type='number']:focus,input[type='password']:focus,input[type='search']:focus,input[type='tel']:focus,input[type='text']:focus,input[type='url']:focus,textarea:focus,select:focus{border-color:#9b4dca;outline:0}select{background:url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="14" viewBox="0 0 29 14" width="29"><path fill="#d1d1d1" d="M9.37727 3.625l5.08154 6.93523L19.54036 3.625"/></svg>') center right no-repeat;padding-right:3.0rem}select:focus{background-image:url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="14" viewBox="0 0 29 14" width="29"><path fill="#9b4dca" d="M9.37727 3.625l5.08154 6.93523L19.54036 3.625"/></svg>')}textarea{min-height:6.5rem}label,legend{display:block;font-size:1.6rem;font-weight:700;margin-bottom:.5rem}fieldset{border-width:0;padding:0}input[type='checkbox'],input[type='radio']{display:inline}.label-inline{display:inline-block;font-weight:normal;margin-left:.5rem}.container{margin:0 auto;max-width:112.0rem;padding:0 2.0rem;position:relative;width:100%}.row{display:flex;flex-direction:column;padding:0;width:100%}.row.row-no-padding{padding:0}.row.row-no-padding>.column{padding:0}.row.row-wrap{flex-wrap:wrap}.row.row-top{align-items:flex-start}.row.row-bottom{align-items:flex-end}.row.row-center{align-items:center}.row.row-stretch{align-items:stretch}.row.row-baseline{align-items:baseline}.row .column{display:block;flex:1 1 auto;margin-left:0;max-width:100%;width:100%}.row .column.column-offset-10{margin-left:10%}.row .column.column-offset-20{margin-left:20%}.row .column.column-offset-25{margin-left:25%}.row .column.column-offset-33,.row .column.column-offset-34{margin-left:33.3333%}.row .column.column-offset-50{margin-left:50%}.row .column.column-offset-66,.row .column.column-offset-67{margin-left:66.6666%}.row .column.column-offset-75{margin-left:75%}.row .column.column-offset-80{margin-left:80%}.row .column.column-offset-90{margin-left:90%}.row .column.column-10{flex:0 0 10%;max-width:10%}.row .column.column-20{flex:0 0 20%;max-width:20%}.row .column.column-25{flex:0 0 25%;max-width:25%}.row .column.column-33,.row .column.column-34{flex:0 0 33.3333%;max-width:33.3333%}.row .column.column-40{flex:0 0 40%;max-width:40%}.row .column.column-50{flex:0 0 50%;max-width:50%}.row .column.column-60{flex:0 0 60%;max-width:60%}.row .column.column-66,.row .column.column-67{flex:0 0 66.6666%;max-width:66.6666%}.row .column.column-75{flex:0 0 75%;max-width:75%}.row .column.column-80{flex:0 0 80%;max-width:80%}.row .column.column-90{flex:0 0 90%;max-width:90%}.row .column .column-top{align-self:flex-start}.row .column .column-bottom{align-self:flex-end}.row .column .column-center{-ms-grid-row-align:center;align-self:center}@media (min-width: 40rem){.row{flex-direction:row;margin-left:-1.0rem;width:calc(100% + 2.0rem)}.row .column{margin-bottom:inherit;padding:0 1.0rem}}a{color:#9b4dca;text-decoration:none}a:focus,a:hover{color:#606c76}dl,ol,ul{list-style:none;margin-top:0;padding-left:0}dl dl,dl ol,dl ul,ol dl,ol ol,ol ul,ul dl,ul ol,ul ul{font-size:90%;margin:1.5rem 0 1.5rem 3.0rem}ol{list-style:decimal inside}ul{list-style:circle inside}.button,button,dd,dt,li{margin-bottom:1.0rem}fieldset,input,select,textarea{margin-bottom:1.5rem}blockquote,dl,figure,form,ol,p,pre,table,ul{margin-bottom:2.5rem}table{border-spacing:0;width:100%}td,th{border-bottom:0.1rem solid #e1e1e1;padding:1.2rem 1.5rem;text-align:left}td:first-child,th:first-child{padding-left:0}td:last-child,th:last-child{padding-right:0}b,strong{font-weight:bold}p{margin-top:0}h1,h2,h3,h4,h5,h6{font-weight:300;letter-spacing:-.1rem;margin-bottom:2.0rem;margin-top:0}h1{font-size:4.6rem;line-height:1.2}h2{font-size:3.6rem;line-height:1.25}h3{font-size:2.8rem;line-height:1.3}h4{font-size:2.2rem;letter-spacing:-.08rem;line-height:1.35}h5{font-size:1.8rem;letter-spacing:-.05rem;line-height:1.5}h6{font-size:1.6rem;letter-spacing:0;line-height:1.4}img{max-width:100%}.clearfix:after{clear:both;content:' ';display:table}.float-left{float:left}.float-right{float:right}
.b-s {  font-size: .8rem;  height: 2.8rem;  line-height: 2.8rem;  padding: 0 1.5rem; position: relative; top:-2px;}
.b-l {border-bottom-right-radius: 0 !important; border-top-right-radius: 0 !important;}
.b-r {border-bottom-left-radius: 0 !important; border-top-left-radius: 0 !important;}
.b-m {border-radius: 0 !important;}
.i-s {max-width: 100px; display: inline-block;height: 2.8rem !important;  line-height: 2.8rem !important; position: relative; top:-1px;}
.i-t {padding-left: 10px; padding-right: 10px; font-size: 80%; display: inline-block;height: 2.8rem !important; border-radius: 3px; line-height: 2.8rem !important; border: 1px solid #d1d1d1;}
h4,h5 {text-align: center;}
.s-c {margin-bottom: 1rem; padding-top: 1rem; border: 1px solid #d1d1d1; border-radius: 1rem; }
.ft {text-align: center;font-size:80%;}
  </style>

  <script type="text/javascript">/*RE:DOM*/!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?t(exports):"function"==typeof define&&define.amd?define(["exports"],t):t((e=e||self).redom={})}(this,(function(e){"use strict";function t(e,t){var i=function(e){for(var t=!1,i=!1,n="",o="",r="",l=0;l<e.length;l++){var s=e[l];"."===s?(i=!0,t=!1,r.length>0&&(r+=" ")):"#"===s?(t=!0,i=!1):t?o+=s:i?r+=s:n+=s}return{tag:n||"div",id:o,className:r}}(e),n=i.tag,o=i.id,r=i.className,l=t?document.createElementNS(t,n):document.createElement(n);return o&&(l.id=o),r&&(t?l.setAttribute("class",r):l.className=r),l}function i(e,t){var i=m(e),o=m(t);return t===o&&o.__redom_view&&(t=o.__redom_view),o.parentNode&&(n(t,o,i),i.removeChild(o)),t}function n(e,t,i){var n=t.__redom_lifecycle;if(o(n))t.__redom_lifecycle={};else{var r=i;for(t.__redom_mounted&&u(t,"onunmount");r;){var l=r.__redom_lifecycle||{};for(var s in n)l[s]&&(l[s]-=n[s]);o(l)&&(r.__redom_lifecycle=null),r=r.parentNode}}}function o(e){if(null==e)return!0;for(var t in e)if(e[t])return!1;return!0}var r=["onmount","onremount","onunmount"],l="undefined"!=typeof window&&"ShadowRoot"in window;function s(e,t,i,o){var s=m(e),a=m(t);t===a&&a.__redom_view&&(t=a.__redom_view),t!==a&&(a.__redom_view=t);var f=a.__redom_mounted,d=a.parentNode;return f&&d!==s&&n(0,a,d),null!=i?o?s.replaceChild(a,m(i)):s.insertBefore(a,m(i)):s.appendChild(a),function(e,t,i,n){for(var o=t.__redom_lifecycle||(t.__redom_lifecycle={}),s=i===n,a=!1,f=0,d=r;f<d.length;f+=1){var h=d[f];s||e!==t&&h in e&&(o[h]=(o[h]||0)+1),o[h]&&(a=!0)}if(!a)return void(t.__redom_lifecycle={});var v=i,c=!1;(s||v&&v.__redom_mounted)&&(u(t,s?"onremount":"onmount"),c=!0);for(;v;){var _=v.parentNode,p=v.__redom_lifecycle||(v.__redom_lifecycle={});for(var w in o)p[w]=(p[w]||0)+o[w];if(c)break;(v.nodeType===Node.DOCUMENT_NODE||l&&v instanceof ShadowRoot||_&&_.__redom_mounted)&&(u(v,s?"onremount":"onmount"),c=!0),v=_}}(t,a,s,d),t}function u(e,t){"onmount"===t||"onremount"===t?e.__redom_mounted=!0:"onunmount"===t&&(e.__redom_mounted=!1);var i=e.__redom_lifecycle;if(i){var n=e.__redom_view,o=0;for(var r in n&&n[t]&&n[t](),i)r&&o++;if(o)for(var l=e.firstChild;l;){var s=l.nextSibling;u(l,t),l=s}}}function a(e,t,i){var n=m(e);if("object"==typeof t)for(var o in t)f(n,o,t[o]);else f(n,t,i)}function f(e,t,i){e.style[t]=null==i?"":i}var d="http://www.w3.org/1999/xlink";function h(e,t,i,n){var o=m(e);if("object"==typeof t)for(var r in t)h(o,r,t[r],n);else{var l=o instanceof SVGElement,s="function"==typeof i;if("style"===t&&"object"==typeof i)a(o,i);else if(l&&s)o[t]=i;else if("dataset"===t)c(o,i);else if(!l&&(t in o||s)&&"list"!==t)o[t]=i;else{if(l&&"xlink"===t)return void v(o,i);n&&"class"===t&&(i=o.className+" "+i),null==i?o.removeAttribute(t):o.setAttribute(t,i)}}}function v(e,t,i){if("object"==typeof t)for(var n in t)v(e,n,t[n]);else null!=i?e.setAttributeNS(d,t,i):e.removeAttributeNS(d,t,i)}function c(e,t,i){if("object"==typeof t)for(var n in t)c(e,n,t[n]);else null!=i?e.dataset[t]=i:delete e.dataset[t]}function _(e){return document.createTextNode(null!=e?e:"")}function p(e,t,i){for(var n=0,o=t;n<o.length;n+=1){var r=o[n];if(0===r||r){var l=typeof r;"function"===l?r(e):"string"===l||"number"===l?e.appendChild(_(r)):y(m(r))?s(e,r):r.length?p(e,r,i):"object"===l&&h(e,r,null,i)}}}function w(e){return"string"==typeof e?b(e):m(e)}function m(e){return e.nodeType&&e||!e.el&&e||m(e.el)}function y(e){return e&&e.nodeType}var g={};function b(e){for(var t,i=[],n=arguments.length-1;n-- >0;)i[n]=arguments[n+1];var o=typeof e;if("string"===o)t=k(e).cloneNode(!1);else if(y(e))t=e.cloneNode(!1);else{if("function"!==o)throw new Error("At least one argument required");var r=e;t=new(Function.prototype.bind.apply(r,[null].concat(i)))}return p(m(t),i,!0),t}var N=b,x=b;function k(e){return g[e]||(g[e]=t(e))}function S(e){for(var t=[],n=arguments.length-1;n-- >0;)t[n]=arguments[n+1];for(var o=function e(t,i,n){for(var o=n,r=new Array(i.length),l=0;l<i.length;l++)r[l]=i[l]&&m(i[l]);for(var u=0;u<i.length;u++){var a=i[u];if(a){var f=r[u];if(f!==o)if(y(f)){var d=o&&o.nextSibling,h=null!=a.__redom_index&&d===r[u+1];s(t,a,o,h),h&&(o=d)}else null!=a.length&&(o=e(t,a,o));else o=o.nextSibling}}return o}(e,t,m(e).firstChild);o;){var r=o.nextSibling;i(e,o),o=r}}b.extend=function(e){for(var t=[],i=arguments.length-1;i-- >0;)t[i]=arguments[i+1];var n=k(e);return b.bind.apply(b,[this,n].concat(t))};var A=function(e,t,i){this.View=e,this.initData=i,this.oldLookup={},this.lookup={},this.oldViews=[],this.views=[],null!=t&&(this.key="function"==typeof t?t:function(e){return function(t){return t[e]}}(t))};function D(e,t,i,n){return new V(e,t,i,n)}A.prototype.update=function(e,t){for(var i=this.View,n=this.key,o=this.initData,r=null!=n,l=this.lookup,s={},u=new Array(e.length),a=this.views,f=0;f<e.length;f++){var d=e[f],h=void 0;if(r){var v=n(d);h=l[v]||new i(o,d,f,e),s[v]=h,h.__redom_id=v}else h=a[f]||new i(o,d,f,e);h.update&&h.update(d,f,e,t),m(h.el).__redom_view=h,u[f]=h}this.oldViews=a,this.views=u,this.oldLookup=l,this.lookup=s};var V=function(e,t,i,n){this.__redom_list=!0,this.View=t,this.initData=n,this.views=[],this.pool=new A(t,i,n),this.el=w(e),this.keySet=null!=i};V.prototype.update=function(e,t){void 0===e&&(e=[]);var n=this.keySet,o=this.views;this.pool.update(e,t);var r=this.pool,l=r.views,s=r.lookup;if(n)for(var u=0;u<o.length;u++){var a=o[u];null==s[a.__redom_id]&&(a.__redom_index=null,i(this,a))}for(var f=0;f<l.length;f++){l[f].__redom_index=f}S(this,l),n&&(this.lookup=s),this.views=l},V.extend=function(e,t,i,n){return V.bind(V,e,t,i,n)},D.extend=V.extend;var j=function(e,t){this.el=_(""),this.visible=!1,this.view=null,this._placeholder=this.el,e instanceof Node?this._el=e:e.el instanceof Node?(this._el=e,this.view=e):this._View=e,this._initData=t};j.prototype.update=function(e,t){var n=this._placeholder,o=this.el.parentNode;if(e){if(!this.visible)if(this._el)s(o,this._el,n),i(o,n),this.el=m(this._el),this.visible=e;else{var r=new(0,this._View)(this._initData);this.el=m(r),this.view=r,s(o,r,n),i(o,n)}this.view&&this.view.update&&this.view.update(t)}else if(this.visible){if(this._el)return s(o,n,this._el),i(o,this._el),this.el=n,void(this.visible=e);s(o,n,this.view),i(o,this.view),this.el=n,this.view=null}this.visible=e};var C=function(e,t,i){this.el=w(e),this.Views=t,this.initData=i};C.prototype.update=function(e,t){if(e!==this.route){var i=this.Views[e];this.route=e,i&&(i instanceof Node||i.el instanceof Node)?this.view=i:this.view=i&&new i(this.initData,t),S(this.el,[this.view])}this.view&&this.view.update&&this.view.update(t,e)};var E="http://www.w3.org/2000/svg",T={};function L(e){for(var t,i=[],n=arguments.length-1;n-- >0;)i[n]=arguments[n+1];var o=typeof e;if("string"===o)t=O(e).cloneNode(!1);else if(y(e))t=e.cloneNode(!1);else{if("function"!==o)throw new Error("At least one argument required");var r=e;t=new(Function.prototype.bind.apply(r,[null].concat(i)))}return p(m(t),i,!0),t}var P=L;function O(e){return T[e]||(T[e]=t(e,E))}L.extend=function(e){var t=O(e);return L.bind(this,t)},L.ns=E,e.List=V,e.ListPool=A,e.Place=j,e.Router=C,e.el=N,e.h=x,e.html=b,e.list=D,e.listPool=function(e,t,i){return new A(e,t,i)},e.mount=s,e.place=function(e,t){return new j(e,t)},e.router=function(e,t,i){return new C(e,t,i)},e.s=P,e.setAttr=function(e,t,i){h(e,t,i)},e.setChildren=S,e.setData=c,e.setStyle=a,e.setXlink=v,e.svg=L,e.text=_,e.unmount=i,Object.defineProperty(e,"__esModule",{value:!0})}));
  </script>
</head>
<body>

</body>
<script>/*TAKATAN*/
let{el:el,mount:mount,setAttr:setAttr,text:text}=redom,vr=el("span"),hd=el("h4#head",["Takatan Servos ver:",vr]),dn=el("input#dn",{type:"text"}),we=el("input#we",{type:"text"}),wp=el("input#wp",{type:"text"}),wma=el("input",{name:"wm",type:"radio"}),wml=el("input",{name:"wm",type:"radio"}),mi=el("input#mi",{type:"text"}),mp=el("input#mp",{type:"text"}),mmo=el("input#mmo",{type:"checkbox"}),wb=el("button","Update Main/WiFi/Master settings"),ft=el(".ft",[el("hr"),el("a","Takatan Software and Robotics 2020",{href:"https://takatan.tk",target:"_blank"}),el("div",[el("span","webpanel builded with: "),el("a","RE:DOM",{href:"https://redom.js.org",target:"_blank"}),el("span",", "),el("a","Milligram",{href:"https://milligram.io",target:"_blank"})])]),$Q=(l,t,s,a)=>{let n=new XMLHttpRequest;n.onreadystatechange=function(){4==n.readyState&&200==n.status&&s(n.responseText)},n.open(t,l,!0);let r=new FormData;if(a)for(e in a)r.append(e,a[e]);n.send(r)},$USI=e=>{},srvs={onoff:[],ang:[],tm:[],min:[],max:[],gt:[],els:[]},$GT=e=>{let l={};l["a"+e]=srvs.ang[e].value,l["t"+e]=srvs.tm[e].value,$Q("/goto","POST",e=>"OK"!=e?alert(e):console.log(e),l)};for(let e=0;e<8;e++)srvs.onoff[e]=el("input",{type:"checkbox"}),srvs.onoff[e].onclick=(l=>$USI(e)),srvs.ang[e]=el("input.i-s.b-m",{type:"text"}),srvs.tm[e]=el("input.i-s.b-m",{type:"text"}),srvs.gt[e]=el("button.b-s.b-r.button-outline","Go"),srvs.gt[e].onclick=(l=>$GT(e)),srvs.min[e]=el("input.i-s.b-m",{type:"text"}),srvs.max[e]=el("input.i-s.b-r",{type:"text"}),srvs.els[e]=el(".container.s-c",[el(".row",[el(".column",[srvs.onoff[e],el("label.label-inline","Servo "+(e+1),{for:"so"+e})])]),el(".row.mb",[el(".column",[el(".i-t.b-l","Go to angle"),srvs.ang[e],el(".i-t.b-m","for"),srvs.tm[e],el(".i-t.b-m","1/10sec"),srvs.gt[e]])]),el(".row.mb",[el(".column",[el(".i-t.b-l","Min angle"),srvs.min[e],el(".i-t.b-m","max angle"),srvs.max[e]])])]);$USI=(e=>{srvs.onoff[e].checked?(srvs.ang[e].disabled=!1,srvs.tm[e].disabled=!1,srvs.gt[e].disabled=!1,srvs.min[e].disabled=!1,srvs.max[e].disabled=!1):(srvs.ang[e].disabled=!0,srvs.tm[e].disabled=!0,srvs.gt[e].disabled=!0,srvs.min[e].disabled=!0,srvs.max[e].disabled=!0)});let sb=el("button","Update Servos settings"),app=el("#app.container",[el(".row",el(".column.column-100",[hd,el("#main",[el("h5","Main"),el(".fl",[el("label",{for:"dn"},"Device Name"),dn])]),el("#wifi",[el("h5","Network"),el(".fl",[el("label","WiFi ESSID",{for:"we"}),we]),el(".fl",[el("label","WiFi pass"),wp]),el(".fl",[el("label","WiFi mode"),[wma,text("Standalone AP"),wml,text("Local Network")]]),el("h5","Master"),el(".fl",[el("label","IP"),mi]),el(".fl",[el("label","Port"),mp]),el(".fl.mb",[el("label.label-inline","Lock"),mmo]),wb])])),el("#servos",[el("h5","Servos"),...srvs.els,sb]),ft]);mount(document.body,app);let updateCFG=()=>{$Q("/cfg","GET",e=>{let l=JSON.parse(e);console.log(["cfg",l]),dn.value=l.name,vr.innerHTML=l.ver,we.value=l.wifi.essid,wp.value=l.wifi.pass,l.wifi.isAP?wma.checked=!0:wml.checked=!0,mi.value=l.master.ip,mp.value=l.master.port,l.master.lock&&(mmo.checked=!0);for(let e=0;e<8;e++)srvs.onoff[e].checked=l.servos[e].on,srvs.ang[e].value=l.servos[e].ca,srvs.tm[e].value=l.servos[e].ct,srvs.min[e].value=l.servos[e].mi,srvs.max[e].value=l.servos[e].mx,$USI(e)})};wb.onclick=(()=>{let e={dn:dn.value,we:we.value,wp:wp.value,wm:wma.checked?"1":"0",mi:mi.value,mp:mp.value,ml:mmo.checked?"1":"0"};$Q("/cfgmain","POST",e=>{"OK"==e?alert("Saved!"):alert("Reconnect?")},e)}),sb.onclick=(()=>{let e={};for(let l=0;l<8;l++)e["o"+l]=srvs.onoff[l].checked?"1":"0",e["a"+l]=srvs.ang[l].value,e["t"+l]=srvs.tm[l].value,e["mi"+l]=srvs.min[l].value,e["mx"+l]=srvs.max[l].value;$Q("/cfgs","POST",e=>{"OK"==e?alert("Saved!"):alert("Not saved =("),location.reload()},e)}),updateCFG();

</script>
</html>)rawliteral";

void readCfg()
{
  EEPROM.begin(sizeof(Cfg));
  EEPROM.get(0,cfg);
}

void writeCfg()
{
  EEPROM.begin(sizeof(Cfg));
  EEPROM.put(0,cfg);
  EEPROM.end();
}

void defCfg()
{
  cfg.ver = 1;
  memset(&cfg.name,0,32);
  memset(&cfg.ssid,0,32);
  memset(&cfg.pass,0,32);
  memset(&cfg.masterIP,0,15);
  String code = String(TF_DEV_PREFIX) + devID;
  strcpy(cfg.name,code.c_str());
  strcpy(cfg.ssid,code.c_str());
  strcpy(cfg.pass,"");
  strcpy(cfg.masterIP,"");
  cfg.isAP = true;
  cfg.masterPort = 0;
  for(int i=0; i<TF_SERVOS;i++)
  {
    cfg.servoEnabled[i] = false;
    cfg.servoClickAngle[i] = TF_DEF_ANGLE;
    cfg.servoClickTime[i] = TF_CLICK_TIME;
    cfg.servoMin[i] = TF_SERVO_MIN_ANGLE;
    cfg.servoMax[i] = TF_SERVO_MAX_ANGLE;
  }
}

void debugCfg(const char * msg)
{
  #ifdef DOSERIAL
    Serial.println("config");
    Serial.print("version: ");
    Serial.println(cfg.ver);
    Serial.print("Device Name: ");
    Serial.println(cfg.name);
    Serial.print("mode: ");
    if (cfg.isAP)
      Serial.println("Standalone AP");
    else
      Serial.println("Local WiFi Network");
    Serial.print("WiFi: ");
    Serial.print(cfg.ssid);
    Serial.print(" [");
    Serial.print(cfg.pass);
    Serial.println("]");
    Serial.print("master [");
    Serial.print(cfg.masterIP);
    Serial.print(":");
    Serial.print(cfg.masterPort);
    Serial.println("]");
    if (cfg.talkOnlyMaster)
      Serial.println("talk only with master [YES]");
    else
      Serial.println("talk only with master [NO]");
    Serial.print(msg);
    Serial.println(" [Done]");
    Serial.println("");
    Serial.println("JSON");
    Serial.println(cfgJSON());
    Serial.println("");
  #endif
}

String cfgJSON()
{
  String ret = "{\"ver\":\""+String(cfg.ver)+"\",\"devID\":\""+devID+"\",\"name\":\""+String(cfg.name)+"\",\"wifi\":{\"isAP\":";
  if (cfg.isAP) ret += "true,";
  else ret += "false,";
  ret += "\"essid\":\""+String(cfg.ssid)+"\",\"pass\":\""+String(cfg.pass)+"\"},\"master\":{\"lock\":";
  if (cfg.talkOnlyMaster) ret += "true,";
  else ret += "false,";
  ret+="\"ip\":\""+String(cfg.masterIP)+"\",\"port\":"+String(cfg.masterPort)+"},\"servos\":[";
  for(int i=0;i<TF_SERVOS;i++)
  {
    if (i>0) ret += ",";
    ret += "{\"on\":";
    if (cfg.servoEnabled[i]) ret += "true,";
    else ret += "false,";
    ret += "\"mi\":"+String(cfg.servoMin[i])+",";
    ret += "\"mx\":"+String(cfg.servoMax[i])+",";
    ret += "\"ca\":"+String(cfg.servoClickAngle[i])+",";
    ret += "\"ct\":"+String(cfg.servoClickTime[i])+"}";
  }
  ret+="]}";
  return ret;
}

void restartWiFi()
{
  debugCfg("restarting WIFI");
  String ssid = String(cfg.ssid);
  String pass = String(cfg.pass);

  if (cfg.isAP)
  {
    WiFi.mode(WIFI_AP);
    if (pass.length())
      WiFi.softAP(ssid,pass);
    else
      WiFi.softAP(ssid);
    #ifdef DOSERIAL
      Serial.print("Standalone Access Point IP address: ");
      Serial.println(WiFi.softAPIP());
    #endif
    return;
  }

  last2LN = millis();
  connecting2LN = true;
  steps2LN = 0;
  WiFi.mode(WIFI_STA);
  if (pass.length())
    WiFi.begin(ssid,pass);
  else
    WiFi.begin(ssid);
  #ifdef DOSERIAL
    Serial.print("connecting ");
  #endif
}

void sendMasterRequest()
{
  if (cfg.isAP||WiFi.status()!=WL_CONNECTED)
    return;
  String ip(cfg.masterIP);
  if (ip=="")
    return;
  String url = "http://"+ip+":"+String(cfg.masterPort)+"/ping/"+devID;
  if(httpRequest.readyState() == 0 || httpRequest.readyState() == 4){
    httpRequest.open("GET", url.c_str());
    httpRequest.send();
  }
}

void masterRequestCB(void*, asyncHTTPrequest*, int){}

bool doLock(AsyncWebServerRequest * r)
{
  if (!cfg.talkOnlyMaster||cfg.isAP)
    return true;
  String mip(cfg.masterIP);
  if (!mip.length())
    return true;
  String cip = r->client()->remoteIP().toString();
  return mip==cip;
}

void setup() {
  #ifdef DOSERIAL
    Serial.begin(TF_SERIAL_SPEED);
    Serial.println("Takatan Servo WeMos [initing]");
  #endif

  String mac = String(WiFi.macAddress());
  devID = mac.substring(9,11) + mac.substring(12,14) + mac.substring(15,17);
  #ifdef DOSERIAL
    Serial.print("Device ID [");
    Serial.print(devID);
    Serial.println("]");
  #endif


  analogWriteFreq(50);  /* Set PWM frequency to 50Hz */

  for(int i=0; i<TF_SERVOS;i++)
  {
    angles[i] = 0;
    clicks[i] = 0;
    //times[i] = 500;
    pinMode(pins[i], OUTPUT);
  }

  pinMode(A0,INPUT);
  int a0 = analogRead(0);
  if (a0>1000)
  {
    bool doFactoryDefaults = true;
    while(doFactoryDefaults&&millis()<5000)//hold 5 sec for reset
    {
      a0 = analogRead(0);
      if (a0<1000)
        doFactoryDefaults = false;
      delay(100);
    }
    if (doFactoryDefaults)
    {
      #ifdef DOSERIAL
        Serial.println("RESET TO FACTORY DEFAULTS!");
      #endif
      defCfg();
      writeCfg();
      pinMode(PIN_BLUE,OUTPUT);
      analogWrite(PIN_BLUE,255);
    }
  }


  readCfg();
  if (cfg.ver!=1)
  {
    defCfg();
    writeCfg();
  }

  restartWiFi();

  // index page
  webserver.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    #ifdef DOSERIAL
      Serial.println("GET /");
    #endif
    if(!doLock(request))
    {
      request->send(403);
      return;
    }
    request->send_P(200, "text/html", index_html);
  });

  // configs JSON data
  webserver.on("/cfg", HTTP_GET, [](AsyncWebServerRequest *request){
    #ifdef DOSERIAL
      Serial.println("GET /cfg");
    #endif
    if(!doLock(request))
    {
      request->send(403);
      return;
    }
    // String ret = devID+"/"+String(TF_SERVOS);
    request->send_P(200, "text/plain", cfgJSON().c_str());
  });

  // save main configs and restart wifi
  webserver.on("/cfgmain", HTTP_POST, [](AsyncWebServerRequest *request){
    #ifdef DOSERIAL
      Serial.println("POST /cfgmain");
    #endif
    if(!doLock(request))
    {
      request->send(403);
      return;
    }
    String nname = String(cfg.name);
    if( request->hasParam("dn", true))
        nname = request->getParam("dn", true)->value();
    String nssid = String(cfg.ssid);
    if (request->hasParam("we", true))
      nssid = request->getParam("we", true)->value();
    String npass = String(cfg.pass);
    if (request->hasParam("wp", true))
      npass = request->getParam("wp", true)->value();

    bool nisAP = cfg.isAP;
    if (request->hasParam("wm", true))
    {
      if (request->getParam("wm", true)->value()=="1")
        nisAP = true;
      else
        nisAP = false;
    }

    String nmi = String(cfg.masterIP);
    if (request->hasParam("mi", true))
      nmi = request->getParam("mi", true)->value();

    String nmps = "0";
    if (request->hasParam("mp", true))
      nmps = request->getParam("mp", true)->value();
    int nmp = nmps.toInt();

    bool nml = cfg.talkOnlyMaster;
    if (request->hasParam("ml",true))
    {
      if (request->getParam("ml",true)->value()=="1")
        nml = true;
      else
        nml = false;
    }
    memset(&cfg.name,0,32);
    memset(&cfg.ssid,0,32);
    memset(&cfg.pass,0,32);
    memset(&cfg.masterIP,0,15);
    strcpy(cfg.name,nname.c_str());
    strcpy(cfg.ssid,nssid.c_str());
    strcpy(cfg.pass,npass.c_str());
    strcpy(cfg.masterIP,nmi.c_str());
    cfg.isAP = nisAP;
    cfg.masterPort = nmp;
    cfg.talkOnlyMaster = nml;
    writeCfg();
    restartWiFi();

    #ifdef DOSERIAL
      Serial.println("main config updated. new config:");
      Serial.println(cfgJSON());
    #endif
    request->send_P(200, "text/plain", "OK");
  });

  //save servos config
  webserver.on("/cfgs", HTTP_POST, [](AsyncWebServerRequest *request){
    #ifdef DOSERIAL
      Serial.println("POST /cfgs");
    #endif
    if(!doLock(request))
    {
      request->send(403);
      return;
    }

    for(int i=0; i<TF_SERVOS;i++)
    {
      String oc = "o"+String(i);
      String ac = "a"+String(i);
      String tc = "t"+String(i);
      String mic = "mi"+String(i);
      String mxc = "mx"+String(i);
      bool no = cfg.servoEnabled[i];
      if (request->hasParam(oc, true))
      {
        if (request->getParam(oc,true)->value()=="1")
          no = true;
        else
          no = false;
      }
      int na = cfg.servoClickAngle[i];
      if (request->hasParam(ac, true))
      {
        String nas = request->getParam(ac,true)->value();
        na = nas.toInt();
      }
      uint nt = cfg.servoClickTime[i];
      if (request->hasParam(tc, true))
      {
        String nts = request->getParam(tc,true)->value();
        nt = (uint)nts.toInt();
      }
      int nmi = cfg.servoMin[i];
      if (request->hasParam(mic, true))
      {
        String nmis = request->getParam(mic,true)->value();
        nmi = (uint)nmis.toInt();
      }
      int nmx = cfg.servoMax[i];
      if (request->hasParam(mxc, true))
      {
        String nmxs = request->getParam(mxc,true)->value();
        nmx = (uint)nmxs.toInt();
      }


      #ifdef DOSERIAL
        Serial.print("s[");
        Serial.print(i);
        Serial.print("] ");
        Serial.print(no);
        Serial.print(" ");
        Serial.print(na);
        Serial.print("/");
        Serial.print(nt);
        Serial.print(" min:");
        Serial.print(nmi);
        Serial.print(" max:");
        Serial.println(nmx);
      #endif

      cfg.servoEnabled[i] = no;
      cfg.servoMin[i] = nmi;
      cfg.servoMax[i] = nmx;
      cfg.servoClickAngle[i] = na;
      cfg.servoClickTime[i] = nt;
    }
    writeCfg();
    request->send_P(200, "text/html", "OK");
  });

  // move any/all servo/s to required angle [for required time!=0]
  webserver.on("/goto", HTTP_POST, [](AsyncWebServerRequest *request){
    #ifdef DOSERIAL
      Serial.println("POST /goto");
    #endif
    if(!doLock(request))
    {
      request->send(403);
      return;
    }

    for(int i=0; i<TF_SERVOS;i++)
    {
      String angleCode = "a"+String(i);
      String timeCode = "t"+String(i);
      if (!request->hasParam(angleCode,true))
        continue;
      int angle = request->getParam(angleCode,true)->value().toInt();
      int gtime = 0;
      if (request->hasParam(timeCode, true))
        gtime = request->getParam(timeCode,true)->value().toInt();
      #ifdef DOSERIAL
        Serial.print("goto[");
        Serial.print(i);
        Serial.print("] ");
        Serial.print(angle);
        Serial.print(" <");
        Serial.print(gtime);
        Serial.println(">");
      #endif
      goTo(i,angle,gtime);
    }
    //writeCfg();
    request->send_P(200, "text/html", "OK");
  });

  webserver.begin();
  httpRequest.onReadyStateChange(masterRequestCB);
  ticker.attach(5, sendMasterRequest);
}

void loop() {

  if (connecting2LN)
  {
    if (millis() - last2LN >= 500)
    {
      last2LN = millis();
      if (WiFi.status()==WL_CONNECTED)
      {
        #ifdef DOSERIAL
          Serial.println(" [OK]");
          Serial.print("local IP [");
          Serial.print(WiFi.localIP());
          Serial.println("]");
        #endif
        //connection done
        connecting2LN = false;
        last2LN = 0;
        steps2LN = 0;
      } else {
        //still not connected
        steps2LN++;
        if (steps2LN >= TF_MAX_CONNECT_TRY)
        {
          //connection trys finished, switch to AP mode
          #ifdef DOSERIAL
            Serial.println(" [FAIL]");
            Serial.println("switching to standalone mode");
          #endif
          cfg.isAP = true;
          pinMode(PIN_BLUE,OUTPUT);
          analogWrite(PIN_BLUE,255);
          restartWiFi();
        } else {
          //make one more try
          #ifdef DOSERIAL
            Serial.print(".");
          #endif
        }
      }
    }
  }

  for(uint8_t i=0; i<TF_SERVOS;i++)
  {
    if (!cfg.servoEnabled[i])
    {
      digitalWrite(pins[i], LOW);
      continue;
    }
    if (clicks[i]!=0&&millis()-clicks[i]>cfg.servoClickTime[i]*100)
    {
      angles[i] = 0;
      clicks[i] = 0;
    }
    int pwm = map(angles[i],cfg.servoMin[i],cfg.servoMax[i],0,255);
    if (pwm < 1)
      pwm = 1;
    if (pwm > 254)
      pwm = 254;
    #ifdef DOSERIAL
      //Serial.println("Servo ["+String(i)+"] pwm "+String(pwm));
    #endif
    analogWrite(pins[i], pwm);
  }

}

void goTo(int fid, int angle, int time)
{
  if (fid >= TF_SERVOS)
    return;
  #ifdef DOSERIAL
    Serial.print("goto : ");
    Serial.println(fid);
  #endif
  angles[fid] = angle;
  if (time==0) {
    clicks[fid] = 0;
  } else {
    clicks[fid] = millis();
    cfg.servoClickTime[fid] = time;
  }
}
